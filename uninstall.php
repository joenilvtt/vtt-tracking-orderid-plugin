<?php

/**
 * Trigger this file if uninstall
 * 
 * @package VTTTrackingOrdeIdPlugin
 */

 if (! defined( 'WP_UNINSTALL_PLUGIN') ) {
    die;
 }

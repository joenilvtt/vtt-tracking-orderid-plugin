<?php

/**
 * @package VTTTrackingOrdeIdPlugin
 */

namespace Inc;

use Page\Admin;

final class Init {

    /**
     * Store all the classes in the array
     */
    public static function get_services () {
        return [
            Pages\Admin::class,
            Base\SettingsLink::class
        ];
    }

    public static function register_services () {
        foreach( self::get_services() as $class ) {
            $service = self::instantiate($class);
            if ( method_exists( $service, 'register' ) ) {
                 $service->register();
            }
        }
    }

    private static function instantiate ($class) {
        return new $class();
    }
}

// use Inc\Base\Activate;
// use Inc\Base\Deactivate;

// if (!class_exists('VTTTrackingOrdeIdPlugin')) {
//     class VTTTrackingOrdeIdPlugin {

//         public $plugin;
    
//         function __construct() {
//             $this->plugin = plugin_basename(__FILE__);
//         }
    
//         public function register() {
//             add_action('init', array($this, 'custom_post_type') );
//             add_action( 'admin_menu', array($this, 'add_admin_page') );
//             add_filter( "plugin_action_links_$this->plugin", array( $this, 'settings_link' ) );
//         }
    
//         public function settings_link( $links ) {
//             // add custome settings link
//             $settingsLink = '<a href="admin.php?page=vtt_tracking_orderid_plugin">Settings</a>';
//             array_push( $links, $settingsLink );
//             return $links;
//         }
    
//         public function add_admin_page() {
//             add_menu_page( 'VTT Tracking OrderId', 'VTT Tracking OrderId', 'manage_options', 'vtt_tracking_orderid_plugin', array($this, 'admin_index'), 'dashicons-store', 110 );
//         }
    
//         public function admin_index() {
//             // require template
//             require_once plugin_dir_path(__FILE__) . 'templates/admin.php';
//         }
    
//         public function custom_post_type () {
//             register_post_type('trackingOrderId', ['public' => true, 'label' => 'Tracking OrderId']);
//         }

//         public function activate () {
//             Activate::activate();
//         }

//         public function deactivate() {
//             Deactivate::deactivate();
//         }
//     }
    
    
    
//     $vttTrackingOrderId = new VTTTrackingOrdeIdPlugin();
//     $vttTrackingOrderId->register();
    
    
//     register_activation_hook(__FILE__, array($vttTrackingOrderId, 'activate'));
    
//     register_deactivation_hook(__FILE__, array($vttTrackingOrderId, 'deactivate'));
// }
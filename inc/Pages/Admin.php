<?php


/**
 * @package VTTTrackingOrdeIdPlugin
 */
namespace Inc\Pages;

use \Inc\Api\SettingsApi;
use \Inc\Base\BaseController;
use Inc\Api\Callbacks\AdminCallbacks;



class Admin extends BaseController {

    public $settings;

    public $callbacks;

    public $pages = array();

    public function register() {

        $this->settings = new SettingsApi();

        $this->callbacks = new AdminCallbacks();

        $this->setPages();

        $this->setSettings();

        $this->setSections();

        $this->setFields();

        $this->settings->addPages( $this->pages )->register();

        add_action('wp_head', array($this, 'insertCodeToHeader'));

    }

    public function insertCodeToHeader() {

        if (!isset($_GET['orderId'])) { return; }

        $ifs_app_id = get_option( 'vtt_infusionsoft_id');
        $ifs_app_key = get_option( 'vtt_infusionsoft_api_key');

        $orderId = $_GET['orderId'];
        // echo $this->plugin_path;
        require($this->plugin_path . 'lib/iSDK/src/isdk.php');

        // $app = new iSDK();

        if ($ifs_app_id && $ifs_app_key && $orderId) {
            // require($this->plugin_path . '/lib/iSDK/src/isdk.php');

            if ($infusionsoft_conn->cfgCon($ifs_app_id, $ifs_app_key)) {
                // $q1 = $app->dsQuery("Invoice", 1, 0, array('Id' => $orderId), array('Id', 'Description', 'InvoiceType', 'ProductSold', 'InvoiceTotal'));
                
                $orderItem = $infusionsoft_conn->dsQuery("OrderItem", 1, 0, array("OrderId" => $orderId), array("Id", "ItemDescription", "ItemName", "PPU", "ProductId", "Qty"));
                // echo json_encode($orderItem[0]);

                $invoiceItem = $infusionsoft_conn->dsQuery("InvoiceItem", 1, 0, array("OrderItemId" => $orderItem[0]["Id"]), array("InvoiceId"));
                // echo json_encode($invoiceItem[0]);

                $invoice = $infusionsoft_conn->dsQuery("Invoice", 1, 0, array("Id" => $invoiceItem[0]["InvoiceId"]), array("InvoiceType", "InvoiceTotal", "JobId"));
                // echo json_encode($invoice[0]);

                $product = $infusionsoft_conn->dsQuery("Product", 1, 0, array("Id" => $orderItem[0]["ProductId"]), array("ProductName", "ProductPrice"));
                // echo json_encode($product[0]);
                
                $itemDescription = "";
                if ( isset( $orderItem[0]["ItemDescription"] ) ) {
                    $itemDescription = $orderItem[0]["ItemDescription"];
                }

                $itemName = "";
                if ( $orderItem[0]["ItemName"] ) {
                    $itemName = $orderItem[0]["ItemName"];
                }

                $ppu = "";
                if ( isset($orderItem[0]["PPU"]) ) {
                    $ppu = $orderItem[0]["PPU"];
                }

                $productId = "";
                if ( isset($orderItem[0]["ProductId"]) ) {
                    $productId = $orderItem[0]["ProductId"];
                }

                $qty = "";
                if ( isset($orderItem[0]["Qty"]) ) {
                    $qty = $orderItem[0]["Qty"];
                }

                $invoiceId = "";
                if ( isset($invoiceItem[0]["InvoiceId"]) ) {
                    $invoiceId = $invoiceItem[0]["InvoiceId"];
                }

                $invoiceType = "";
                if ( isset($invoice[0]["InvoiceType"]) ) {
                    $invoiceType = $invoice[0]["InvoiceType"];
                }

                $invoiceTotal = "";
                if ( isset($invoice[0]["InvoiceTotal"]) ) {
                    $invoiceTotal = $invoice[0]["InvoiceTotal"];
                }
                
                $jobId = "";
                if ( isset($invoice[0]["JobId"]) ) {
                    $jobId = $invoice[0]["JobId"] ;
                }

                $productName = "";
                if ( isset($product[0]["ProductName"]) ) {
                    $productName = $product[0]["ProductName"];
                }

                $productPrice = "";
                if ( isset($product[0]["ProductPrice"]) ) {
                    $productPrice = $product[0]["ProductPrice"];
                }

                ?>
                <script>
                    window.eTracking = {
                        orderId: "<?php echo $orderId ?>",
                        itemDescription: "<?php echo $itemDescription; ?>",
                        itemName: "<?php echo $itemName; ?>",
                        ppu: "<?php echo $ppu;  ?>",
                        productId: "<?php echo $productId; ?>",
                        qty: "<?php echo $qty; ?>",
                        invoiceId: "<?php echo $invoiceId; ?>",
                        invoiceType: "<?php echo $invoiceType; ?>",
                        invoiceTotal: "<?php echo $invoiceTotal ?>",
                        jobId: "<?php echo $jobId; ?>",
                        productName: "<?php echo $productName; ?>",
                        productPrice: "<?php echo $productPrice;?>"
                    };
                </script>
                <?php
            }
        }
    }

    // require_once("$this->plugin_path/templates/admin.php");
    public function setPages() {
        $this->pages = array(
            array(
                'page_title' => 'VTT Tracking',
                'menu_title' => 'VTT Tracking',
                'capability' => 'manage_options',
                'menu_slug' => 'vtt_tracking_orderid_plugin',
                'callback' => array( $this->callbacks, 'adminDashboard'),
                'icon_url' => 'dashicons-store',
                'position' => 110,
            )
        );
    }

    public function setSettings() {
        $args = array(
            array(
                'option_group' => 'vtt_options_group',
                'option_name' => 'vtt_infusionsoft_id',
                'callback' => array($this->callbacks, 'vttOptionsGroup')
            ),
            array(
                'option_group' => 'vtt_options_group',
                'option_name' => 'vtt_infusionsoft_api_key'
            )
        );

        $this->settings->setSettings( $args );
    }

    public function setSections() {
        $args = array(
            array(
                'id' => 'vtt_admin_index',
                'title' => 'Settings',
                'callback' => array($this->callbacks, 'vttAdminSection'),
                'page' => 'vtt_tracking_orderid_plugin'
            )
        );

        $this->settings->setSections( $args );
    }

    public function setFields() {
        $args = array(
            array(
                'id' => 'vtt_infusionsoft_id',
                'title' => 'Infusionsoft App ID',
                'callback' => array($this->callbacks, 'vttInfusionsoftId'),
                'page' => 'vtt_tracking_orderid_plugin',
                'section' => 'vtt_admin_index',
                'args' => array(
                    'label_for' => 'vtt_infusionsoft_id',
                    'class' => 'example-class'
                )
            ),
            array(
                'id' => 'vtt_infusionsoft_api_key',
                'title' => 'Infusionsoft API Key',
                'callback' => array($this->callbacks, 'vttInfusionsoftApiKey'),
                'page' => 'vtt_tracking_orderid_plugin',
                'section' => 'vtt_admin_index',
                'args' => array(
                    'label_for' => 'vtt_infusionsoft_api_key',
                    'class' => 'example-class'
                )
            )
        );

        $this->settings->setFields( $args );
    }
}
<?php
/**
 * @package VTTTrackingOrdeIdPlugin
 */

namespace Inc\Base;

class BaseController {

    public $plugin_path;

    public $plugin;

    public function __construct() {
        // $this->plugin_path = "/Applications/MAMP/htdocs/wordpress/wp-content/plugins/vtt-tracking-orderid-plugin";
        // $this->plugin = '/Applications/MAMP/htdocs/wordpress/wp-content/plugins/vtt-tracking-orderid-plugin/vtt-tracking-orderid-plugin.php';
        $this->plugin_path = plugin_dir_path( dirname( dirname( __FILE__ ) ) );
        $this->plugin = plugin_basename( dirname( dirname( dirname( __FILE__ ) ) ) ) . '/vtt-tracking-orderid-plugin.php';
    }
}
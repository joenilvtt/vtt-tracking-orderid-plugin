<?php
/**
 * @package VTTTrackingOrdeIdPlugin
 */

namespace Inc\Base;

use \Inc\Base\BaseController;

class SettingsLink extends BaseController {

    public function register () {
        add_filter( "plugin_action_links_" . $this->plugin, array( $this, 'settings_link' ) );
    }

    public function settings_link ( $links  ) {
        $settingsLink = '<a href="admin.php?page=vtt_tracking_orderid_plugin">Settings</a>';
        array_push( $links, $settingsLink );
        return $links;
    }
}
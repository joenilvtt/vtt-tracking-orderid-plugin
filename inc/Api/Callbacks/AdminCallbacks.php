<?php
/**
 * @package VTTTrackingOrdeIdPlugin
 */

namespace Inc\Api\Callbacks;

use Inc\Base\BaseController;

class AdminCallbacks extends BaseController {
    public function adminDashboard() {
        return require_once("$this->plugin_path/templates/admin.php");
    }

    public function vttOptionsGroup ( $input ) {
        return $input;
    }

    public function vttAdminSection () {
        echo "Please fill out the fields.";
    }

    public function vttInfusionsoftId () {
        $value = esc_attr( get_option( 'vtt_infusionsoft_id') );
        echo '<input type="text" class="regular-text" name="vtt_infusionsoft_id" value="' . $value . '" placeholder="Infusionsoft App ID">';
    }

    public function vttInfusionsoftApiKey () {
        $value = esc_attr( get_option( 'vtt_infusionsoft_api_key') );
        echo '<input type="text" class="regular-text" name="vtt_infusionsoft_api_key" value="' . $value . '" placeholder="Infusionsoft API key">';
    }
}